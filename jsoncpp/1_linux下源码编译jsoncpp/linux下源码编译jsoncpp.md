## **1.jsoncpp编译和安装**

下载源码

```shell
git clone https://github.com/open-source-parsers/jsoncpp
```

进入源码目录

```shell
cd jsoncpp/
```

创建编译目录`build`  和安装目录`install`

```shell
mkdir build  install
```

编译安装

>不安装到系统默认目录，安装到自己指定的目录`../install`中  
>不编译动态库，只编译静态库，通过`-DBUILD_STATIC_LIBS=ON -DBUILD_SHARED_LIBS=OFF `设置。

```shell
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install  \
 -DCMAKE_BUILD_TYPE=RELEASE  \
 -DBUILD_STATIC_LIBS=ON \
 -DBUILD_SHARED_LIBS=OFF ..
 
make
make install
```

## 2.编写简单的测试

编写test.cpp如下：

```c++
#include "json/json.h"

int main(int argc, const char *argv[])
{
	Json::Value jsonReq;
	return 0;
}
```

编写Makefile文件并指定从jsoncpp的安装路径中寻找头文件和库文件，根据自己的目录作修改

```makefile
TARGET=test
$(TARGET):test.cpp
	g++ $^ -o $@ \
		-I./jsoncpp/install/include \
		./jsoncpp/install/lib/libjsoncpp.a 
.PHONY:clean
clean:
	rm -f  $(TARGET)
```

编译通过，接下来可以使用jsoncpp了



