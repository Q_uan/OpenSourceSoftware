﻿#include "json/json.h"
#include<iostream>
#include<fstream>
using namespace std;

void ReadFileJson()
{
    Json::Value root;//定义根节点
    Json::Reader reader;

    ifstream in("./students.json", ios::binary);
    if (!in.is_open())
    {
        cout << "文件打开错误"<<endl;
        return;
    }

    if (reader.parse(in, root))
    {
        string school = root["school"].asString();
        string classNum = root["classNum"].asString();
		cout<<"school:"<<school<<endl;
		cout<<"classNum:"<<classNum<<endl;

        int size = root["students"].size();
        for (int i = 0; i < size; i++)
        {
    		string name = root["students"][i]["name"].asString();
    		string age = root["students"][i]["age"].asString();
    		cout<<"stuent "<<name<<" "<<age<<" years old."<<endl;
        }
    }
    else
    {
        cout << "json文件解析错误"<<endl;
    }
    
}

int main(int argc, const char *argv[])
{
    ReadFileJson();
    return 0;
}
