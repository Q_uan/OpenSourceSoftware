#!/bin/bash

export HOST=
export CROSS=
export CC=${CROSS}gcc
export AR=${CROSS}ar
export CPP=${CROSS}cpp
export CXX=${CROSS}g++
export STRIP=${CROSS}strip
export RANLIB=${CROSS}ranlib

CUR_PATH=`pwd`
INSTALL_PATH=$CUR_PATH/build/x86    #安装目录

export CFLAGS=-I$INSTALL_PATH/include
export LDFLAGS=-L$INSTALL_PATH/lib

####for grpc####
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_PATH/lib/"
export LIBRARY_PATH="$LIBRARY_PATH:$INSTALL_PATH/lib/"
export CPATH="$CPATH:$INSTALL_PATH/include/"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_PATH/lib/pkgconfig"

function build_zlib()
{
    mkdir -p $INSTALL_PATH
    tar -xvf zlib-1.2.13.tar.gz
    cd zlib-1.2.13

    ./configure --prefix=$INSTALL_PATH
    make -j
    make install
    cd ..
}

function build_openssl()
{
    mkdir -p $INSTALL_PATH
    tar -xvf openssl-OpenSSL_1_1_1u.tar.gz
    cd openssl-OpenSSL_1_1_1u

    ./config no-asm shared --prefix=$INSTALL_PATH --cross-compile-prefix=$CROSS
    make -j
    make install
    cd ..
}

function build_gflags()
{
    mkdir -p $INSTALL_PATH
    tar -xvf  gflags-2.2.1.tar.gz
    cd gflags-2.2.1

    rm CMakeCache.txt
    cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CXX -DGFLAGS_BUILD_SHARED_LIBS=ON -DBUILD_SHARED_LIBS=on -DBUILD_STATIC_LIBS=on
    make -j
    make install
    cd ..
}

function build_protobuf()
{
    mkdir -p $INSTALL_PATH
    tar -xvf protobuf-3.13.0.tar.gz
    cd protobuf-3.13.0

    ./autogen.sh
    ./configure --prefix=$INSTALL_PATH --host=$HOST --with-protoc=
    make 
    make install
    cd ..
}

function build_c-ares()
{
    mkdir -p $INSTALL_PATH
    tar -xvf c-ares-1.14.0.tar.gz
    cd c-ares-1.14.0

    ./buildconf
    cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CXX
    make
    make install
    cd ..
}

function build_grpc()
{
    mkdir -p $INSTALL_PATH
    tar -xvf grpc-1.30.0.tar.gz
    cd grpc-1.30.0 

    rm CMakeCache.txt
    cmake  -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}            \
                            -DCMAKE_INSTALL_LIBDIR=${INSTALL_PATH}/lib           \
                            -DCMAKE_FIND_ROOT_PATH=${INSTALL_PATH}               \
                            -DTARGET_LINK_LIBRARIES:STRING=${INSTALL_PATH}/lib   \
                            -DLINK_LIBRARIES:STRING=${INSTALL_PATH}/lib          \
                            -DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX} \
                            -DgRPC_ZLIB_PROVIDER:STRING=package                  \
                            -DgRPC_CARES_PROVIDER:STRING=package                 \
                            -DgRPC_PROTOBUF_PROVIDER:STRING=package              \
                            -DgRPC_PROTOBUF_PACKAGE_TYPE:STRINGS=MODULE          \
                            -DgRPC_SSL_PROVIDER:STRING=package                   \
                            -DgRPC_GFLAGS_PROVIDER:STRING=package                \
                            -DPROTOBUF_PROTOC_EXECUTABLE:FILEPATH=${INSTALL_PATH}/bin/protoc \
                            -DgRPC_BUILD_GRPC_PYTHON_PLUGIN=OFF                  \
                            -DgRPC_BUILD_TESTS=OFF -DBUILD_SHARED_LIBS:BOOL=ON
    make
    make install
    cd ..
}

function buildAll()
{
    build_zlib
    build_openssl
    build_gflags
    build_protobuf
    build_c-ares
    build_grpc
}

#############################
#func  :清除编译
#############################
function clean()
{
    rm -rf $INSTALL_PATH
    rm -rf zlib-1.2.13
    rm -rf openssl-OpenSSL_1_1_1u
    rm -rf gflags-2.2.1
    rm -rf protobuf-3.13.0
    rm -rf c-ares-1.14.0
    rm -rf grpc-1.30.0
}

#########################
#func: 菜单
#########################
function menu()
{

    if [ $# -ne 1 ]
    then
        echo "usage : $0 build/clean"
        exit
    fi

    if   [ $1 = "build" ]
    then
        buildAll
    elif [ $1 = "clean" ]
    then
        clean
    else
        echo "usage : $0 build/clean"
    fi
}

function main()
{
    menu $*
}
main $*