# 1.Apache base64介绍

在stackoverflow的一个[关于base64的讨论](https://stackoverflow.com/questions/342409/how-do-i-base64-encode-decode-in-c)总列举了多种不同的base64编码实现并对比了编解码速度，其中[Apache版本的base64]( https://opensource.apple.com/source/QuickTimeStreamingServer/QuickTimeStreamingServer-452/CommonUtilitiesLib/ )具有较好的性能，并且这一实现中提供了计算编解码后数据长度的API，这将更便于使用。

# 2.Apache base64提供了哪些API？

| 函数             | 说明                                                     |
| ---------------- | -------------------------------------------------------- |
| Base64encode_len | 返回编码 base64 字符串所需的最大字符数（包含\0），估计值 |
| Base64encode     | 编码，返回值为包含\0的编码字符串长度                     |
| Base64decode_len | 返回解码 base64 字符串所需的最大字节数，估计值           |
| Base64decode     | 解码，返回值为不包含\0的明文长度                           |

# 3.使用注意事项

## 3.1 Base64encode_len

返回编码 base64 字符串所需的最大字符数，**计算结果包含存放结束标志\0**，例如 

```c
const char * str = "123";           /* str编码后为："MTIz" */
int len = strlen(str);              /* str的字符长度为3 */
int outLen = Base64encode_len(len); /* outLen的长度>=5 */
```

## 3.2 Base64encode

`Base64encode`的**返回值是编码后长度（包含\0）**

`Base64encode`**会自动在末尾写入结束标志\0**。

## 3.3 Base64decode_len

Base64decode_len的返回值既不是**“解码后字符串的长度”**也不是**“包含结束标志\0的解码后长度”**，而是**会比“包含结束标志\0的解码后长度”多一到两个字节的长度**。

如果被编码的数据的长度不能很好地排列成 3 的倍数，Base64 使用 '=' 字符添加填充，填充 '=' 的个数可能为0、1、2，因此推测Base64decode_len的返回值和这个原因有关，**Base64decode_len的返回值是能保证容纳解码后数据长度的估计值**。

- [x] 注意：Base64decode_len返回的不是解码后字符串的长度

# 3.4 Base64decode

Base64decode的返回值是**“不包含结束标志\0的解码后长度”**，也就是不包含\0的解码后的明文长度。



# 4.测试用例



# 5.关于Get original length from a Base 64 string

Get original length from a Base 64 string
https://blog.aaronlenoir.com/2017/11/10/get-original-length-from-base-64-string/
