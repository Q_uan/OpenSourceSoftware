#include <cstdio>
#include "event2/event.h"

int main(int argc, const char *argv[])
{
    struct event_base * base = event_base_new();
    if(nullptr == base)
    {
        printf("Fail to event_base_new\n");
        return -1;
    }

    printf("Using libevent with backend method: %s\n", event_base_get_method(base));

    enum event_method_feature f = (event_method_feature)event_base_get_features(base);
    if(f & EV_FEATURE_ET)
    {
        printf("Edge triggered events are supported.\n");
    }
    if(f & EV_FEATURE_O1)
    {
        printf("O(1) event notification is supported.\n");
    }
    if(f & EV_FEATURE_FDS)
    {
        printf("All fd types are supported.\n");
    }
    return 0;
}
