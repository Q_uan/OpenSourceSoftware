#include <iostream>
#include "UdpServer.h"
#include <thread>
#include <signal.h>
#ifdef _WIN32
    #pragma comment(lib,"ws2_32.lib")
#endif

static volatile bool g_runFlag = true;
void sighandler(int signum)
{
    g_runFlag = false;
}

static void callback(struct sockaddr_in from, unsigned char * buf, int size, void * arg)/* 设置读取数据回调函数 */
{
    char addr[16] = {0};
    evutil_inet_ntop(AF_INET, &from.sin_addr, addr, sizeof(addr));
    printf("Recv from %s:%d", addr, ntohs(from.sin_port));
    printf("data: %s", buf);
}

int main(int argc, const char *argv[])
{

#ifdef _WIN32
{
    WORD wVersionRequested;
    WSADATA wsaData;
    wVersionRequested = MAKEWORD(2, 2);
    WSAStartup(wVersionRequested, &wsaData);
}
#endif

    UdpServer server;
    server.init("0.0.0.0", 9999);
    server.setReadHandler(callback);
    server.start();

    signal(SIGINT, sighandler);

    while (g_runFlag)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }

#ifdef _WIN32
    WSACleanup();
#endif
    return 0;
}
