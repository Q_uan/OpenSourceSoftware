#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <event2/event.h>
#include <event2/bufferevent.h>
#include <arpa/inet.h>

void read_cb(struct bufferevent *bev, void *arg)
{
    char buf[1024] = {0};
    bufferevent_read(bev, buf, sizeof(buf));
    printf("recv:%s\n", buf);
    bufferevent_write(bev, buf, strlen(buf) +1);
    //sleep(1);
}

void write_cb(struct bufferevent *bev, void *arg)
{
    printf("write call back\n");
}

void event_cb(struct bufferevent *bev, short events, void * arg)
{
    if(events & BEV_EVENT_EOF)
    {
        printf("connection closed \n");
    }
    else if(events & BEV_EVENT_ERROR)
    {
        printf("some other error\n");
    }
    else if(events & BEV_EVENT_CONNECTED)
    {
        printf("connect to server ok.....\n");
        return ;
    }

    //释放资源
    bufferevent_free(bev);
}

//客户端与用户教会，从终端读取数据给服务器
void read_terminal(evutil_socket_t fd, short what, void *arg)
{
    //读数据
    char buf[1024] = {0};
    int len = read(fd, buf, sizeof(buf));

    struct bufferevent* bev = (struct bufferevent*)arg;

    //发送数据
    bufferevent_write(bev, buf, len+1);
}
int main()
{
    struct event_base* base = event_base_new();

    /* 通信的fd放到bufferevent中,这里设置-1 */
    struct bufferevent *bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);

    struct sockaddr_in serv = {0};
    serv.sin_family = AF_INET;
    serv.sin_port = htons(9999);
    serv.sin_addr.s_addr = inet_addr("192.168.11.156");  //设置地址

    //设置回调
    bufferevent_setcb(bev, read_cb, write_cb, event_cb, NULL);
    
    //设置回调生效
    bufferevent_enable(bev, EV_READ|EV_WRITE);

    //连接服务器
    bufferevent_socket_connect(bev, (struct sockaddr*)&serv, sizeof(serv));

    //创建标准输入事件
    struct event* ev = event_new(base, STDIN_FILENO, EV_READ | EV_PERSIST, read_terminal, bev);

    //添加事件
    event_add(ev, NULL);
    event_base_dispatch(base);
    event_free(ev);
    event_base_free(base);
}
