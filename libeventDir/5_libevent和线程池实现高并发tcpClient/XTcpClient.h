#pragma once
#include <event2/event.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <string>
#include <pthread.h>
#include "XTcpTask.h"

class XTcpClient
{
public:
    static XTcpClient* NewInstance(int threadCount);
    ~XTcpClient();
    bool addTask(XTask *task);
private:
    int m_threadCount = 0;/* 任务处理线程池的线程数 */
    event_base * m_base = NULL;
    pthread_t m_threadID;          /* 线程ID */
    bool m_isRunning = false;      /* 线程运行标志 */
private:
    XTcpClient(int threadCount);
    bool construct();
    static void *PollThread(void*arg);
};
