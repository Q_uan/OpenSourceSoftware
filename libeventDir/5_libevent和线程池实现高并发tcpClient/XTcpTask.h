#pragma once
#include "XTask.h"
#include <string>

class XTcpTask :public XTask
{
    std::string m_serverIp;
    std::string m_serverPort;
public:
    virtual bool Init();    //初始化任务
    XTcpTask(std::string serverIp, std::string serverPort);
    ~XTcpTask();
private:
    static void eventCallBack(struct bufferevent *bev, short what, void *arg);
    static void readCallBack(bufferevent * bev, void *arg);
};

