#pragma once

#include <event2/event.h>
#include <list>
#include <mutex>

class XTask;

class XThread
{
public:
    void Start();          //启动线程
    void AddTask(XTask *t);//添加处理的任务，一个线程同时可以处理多个任务，共用一个event_base
    void Activate();       //线程激活
    XThread();
    ~XThread();
    int getId();
    bool setId(int id);
private:
    void run();      //线程入口函数
    bool Setup();    //安装线程，初始化event_base和管道监听事件用于激活
    static void NotifyCB(evutil_socket_t fd, short which, void *arg);
    void Notify(evutil_socket_t fd, short which);//收到主线程发出的激活消息（线程池的分发）
private:
    int m_Id = -1;//线程编号
    int m_pipeWriteFd = 0;
    struct event_base *m_base = 0;
    std::list<XTask*> m_taskList;//任务列表
    std::mutex m_mutex;//线程安全 互斥
};

