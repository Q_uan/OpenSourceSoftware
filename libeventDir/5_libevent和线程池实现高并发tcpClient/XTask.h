#pragma once

class XTask
{
protected:
    int m_fd = -1;
    struct event_base *m_base = nullptr;
public:
    bool setFd(int fd)
    {
        m_fd = fd;
        return true;
    }

    bool setBase(struct event_base *base)
    {
        m_base = base;
        return true;
    }
    virtual bool Init() = 0;    //初始化任务
};

