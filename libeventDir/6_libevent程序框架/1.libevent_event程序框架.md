## 一.libevent程序框架

使用libevent库编写一个程序，通常遵循以下流程

1. 创建event_base

```c
struct event_base *event_base_new(void);
```

2. 创建event(事件)

   事件分为两种 :`event`和带缓冲的`bufferevent`
   
   event事件的创建：

```c
struct event *event_new(struct event_base *base, evutil_socket_t fd, short events, void (*cb)(evutil_socket_t, short, void *), void *arg)
```

​		bufferevent的创建：

```c
struct bufferevent *bufferevent_socket_new(struct event_base *base, evutil_socket_t fd, int options);
```

   3.将事件添加到base上


   ```c
int event_add(struct event *ev, const struct timeval *timeout);
   ```

4. 循环监听事件是否满足

```c
int event_base_dispatch(struct event_base *event_base)
```

5.   释放事件

```c
 void event_free(struct event *ev)
```

6. 释放event_base

```c
void event_base_free(struct event_base *base)
```

