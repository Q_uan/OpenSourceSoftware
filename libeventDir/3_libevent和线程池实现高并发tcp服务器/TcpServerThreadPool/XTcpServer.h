#pragma once
#include <event2/event.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <string>
#include <pthread.h>

class XTcpServer
{
public:
    static XTcpServer* NewInstance(int threadCount, std::string ip, int port);
    ~XTcpServer();
private:
    struct XTcpServerArgs
    {
        int port = 0;       /* 端口号 */
        std::string  ip;    /* ip */
        int threadCount = 0;/* 任务处理线程池的线程数 */
        event_base * base = NULL;
        evconnlistener * evConnListener = NULL;
        pthread_t threadID;          /* 线程ID */
        bool isRunning = false;      /* 线程运行标志 */
    };
    XTcpServerArgs m_args;   /* 线程参数 */
private:
    XTcpServer(int threadCount, std::string ip, int port);
    bool construct();
    static void listenCallback(struct evconnlistener * e, evutil_socket_t s, struct sockaddr *a, int socklen, void *arg);
    static void *PollThread(void*arg);
};
