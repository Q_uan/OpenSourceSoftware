#pragma once
#include "XTask.h"

class XTcpTask :public XTask
{
public:
    virtual bool Init();    //初始化任务
    XTcpTask(int fd);
    ~XTcpTask();
private:
    static void eventCallBack(struct bufferevent *bev, short what, void *arg);
    static void readCallBack(bufferevent * bev, void *arg);
};

