#pragma once
#include <vector>
class XThread;
class XTask;

class XThreadPool
{
public:
    static XThreadPool* GetInstance();/* 单件模式 */
    bool Init(int threadCount);       /* 初始化所有线程并启动线程 */
    void Dispatch(XTask *task);       /* 分发线程 */
private:
    XThreadPool();                    /* 将构造函数访问属性设置为private,禁止外部调用 */
    XThreadPool(const XThreadPool&);  /* 禁止外部调用拷贝构造函数 */
    XThreadPool& operator= (const XThreadPool&);/* 禁用赋值操作符 */
private:
    int threadCount = 0;                        /* 线程数量 */
    int lastThread = -1;
    std::vector<XThread *>threads;              /* 线程池线程 */
};

