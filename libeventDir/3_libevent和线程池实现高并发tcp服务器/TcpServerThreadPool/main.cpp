﻿#include <signal.h>
#include <iostream>
#include <string.h>
#include "XTcpServer.h"
#include <unistd.h>

using namespace std;

int main()
{
    //忽略管道信号，发送数据给已关闭的socket
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
    {
        return 1;
    }

    XTcpServer * pServer = XTcpServer::NewInstance(10, "0.0.0.0", 9999); 

    while(true)
    {
        sleep(1);
    }
    delete pServer;
    return 0;
}
