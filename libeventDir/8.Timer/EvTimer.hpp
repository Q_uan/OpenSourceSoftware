/*
 * Copyright (c) 20023-2028 quanqixian
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _EV_TIMER_H_
#define _EV_TIMER_H_

#include "event2/event.h"
#include "event2/thread.h"
#include <functional>
#include <thread>
#include <mutex>

#define EVTIM_LOG_FATAL(errnum, fmt, ...) printf("[F] %s:%d [%s] errnum=%d " fmt "\n", __FILE__, __LINE__, __FUNCTION__, errnum, ##__VA_ARGS__)
#define EVTIM_LOG_ERROR(errnum, fmt, ...) printf("[E] %s:%d [%s] errnum=%d " fmt "\n", __FILE__, __LINE__, __FUNCTION__, errnum, ##__VA_ARGS__)
#define EVTIM_LOG_WARN(errnum, fmt, ...)  printf("[W] %s:%d [%s] errnum=%d " fmt "\n", __FILE__, __LINE__, __FUNCTION__, errnum, ##__VA_ARGS__)
#define EVTIM_LOG_INFO(fmt, ...)          printf("[I] %s:%d [%s] " fmt "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#define EVTIM_LOG_DEBUG(fmt, ...)         printf("[D] %s:%d [%s] " fmt "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#define EVTIM_LOG_TRACE(fmt, ...)         printf("[T] %s:%d [%s] " fmt "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

class EvTimer
{
public:
    EvTimer()
    {
    #ifdef _WIN32
        evthread_use_windows_threads();
    #else
        evthread_use_pthreads();
    #endif
    }

    template <class Fn, class... Args>
    explicit EvTimer(const timeval & timeout, bool isPersist, Fn && fn, Args&&... args)
        : m_timeout(timeout), m_isPersist(isPersist)
    {
        m_callback = std::bind(fn, args...);
    #ifdef _WIN32
        evthread_use_windows_threads();
    #else
        evthread_use_pthreads();
    #endif
    }
    ~EvTimer()
    {
        stop();
    }

    bool start()
    {
        bool ret = false;

        std::lock_guard<std::mutex> locker(m_mutex);

        if(nullptr == m_callback)
        {
            return false;
        }

        if(m_isRunning)
        {
            EVTIM_LOG_ERROR(-1, "Timer(%ld) alaredy started.", (long)this);
            return false;
        }

        do
        {
            m_base = event_base_new();
            if(nullptr == m_base)
            {
                EVTIM_LOG_ERROR(-1, "Timer(%ld) fail to event_base_new.", (long)this);
                break;
            }

            int flags = m_isPersist ? EV_PERSIST : 0;
            m_event = event_new(m_base, -1, flags, timeoutCB, this);
            if(nullptr == m_event)
            {
                EVTIM_LOG_ERROR(-1, "Timer(%ld) fail to event_new.", (long)this);
                break;
            }
            event_add(m_event, &m_timeout);

            m_thread = new (std::nothrow) std::thread(&EvTimer::dispatchThread, this);
            if(nullptr == m_thread)
            {
                EVTIM_LOG_ERROR(0, "Timer(%ld) dispatch thread create fail", (long)this);
                m_isRunning = false;
                break;
            }
            EVTIM_LOG_WARN(0, "Timer(%ld) start ok", (long)this);
            m_isRunning = true;
            ret = true;
        }while(0);

        if(!ret)
        {
            if(m_event)
            {
                event_free(m_event);
                m_event = nullptr;
            }
            if(m_base)
            {
                event_base_free(m_base);
                m_base = nullptr;
            }
            if(m_thread)
            {
                m_thread->join();
                delete m_thread;
                m_thread = nullptr;
            }
        }

        return ret;
    }

    template <class Fn, class... Args>
    bool start(const timeval & timeout, bool isPersist, Fn && fn, Args&&... args)
    {
        {
            std::lock_guard<std::mutex> locker(m_mutex);
            m_timeout = timeout;
            m_isPersist = isPersist;
            m_callback = std::bind(fn, args...);
        }

        return start();
    }
    bool stop()
    {
        std::lock_guard<std::mutex> locker(m_mutex);

        if(m_isRunning)
        {
            m_isRunning = false;
            event_base_loopbreak(m_base);
            event_base_loopexit(m_base, nullptr);
            if(m_thread)
            {
                m_thread->join();
                delete m_thread;
                m_thread = nullptr;
            }
            if(m_event)
            {
                event_free(m_event);
                m_event = nullptr;
            }
            if(m_base)
            {
                event_base_free(m_base);
                m_base = nullptr;
            }
            EVTIM_LOG_WARN(0, "Timer(%ld) stoped", (long)this);
        }

        return (false == m_isRunning);
    }
public:
    long getID(){ return (long)this;}
private:
    EvTimer(const EvTimer &) = delete;
    EvTimer& operator = (const EvTimer&) = delete;
private:
    void * dispatchThread()
    {
        if(m_base)
        {
            event_base_dispatch(m_base);
        }
        return nullptr;
    }
    static void timeoutCB(evutil_socket_t fd, short event, void *arg)
    {
        EvTimer * pThis = static_cast<EvTimer *>(arg);
        pThis->m_callback();
    }
private:
    std::mutex m_mutex;
    std::thread *m_thread = nullptr;
    bool m_isRunning = false; 
    event_base* m_base = nullptr;
    struct event * m_event = nullptr;
private:
    timeval m_timeout{};
    std::function<void() > m_callback = nullptr;
    bool m_isPersist = false;
};

#endif
