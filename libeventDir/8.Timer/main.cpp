#include <iostream>
#include "EvTimer.hpp"

void func01()
{
    std::cout<<"static function"<<std::endl;
}

int main(int argc, const char *argv[])
{
    /* static function */
    EvTimer timer01;
    timer01.start({1, 0}, true, func01);

    /* lambda */
    EvTimer timer02;
    timer02.start({1,0}, true,[]()->void{
            std::cout<<"lambda"<<std::endl;
            });

    /* no static class member function */
    class Test
    {
    public:
        void print(){
            std::cout<<"no static class member function"<<std::endl;
        }
    };
    EvTimer timer03;
    Test test;
    timer03.start({1,0}, true, &Test::print, &test);

    /* Variable parameter  */
    EvTimer timer04;
    timer04.start({1,0},true,[](int a, void* arg)->void{
            std::cout<<"variable parameter, a="<<a<<std::endl;
            }, 1, nullptr);

    std::this_thread::sleep_for(std::chrono::seconds(5));
    return 0;
}
