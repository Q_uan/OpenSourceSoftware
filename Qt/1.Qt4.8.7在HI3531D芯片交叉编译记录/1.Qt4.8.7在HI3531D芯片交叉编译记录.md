说明：无OpenGL支持

## 1.Qt交叉编译

### 1.1下载源码

在http://download.qt.io/archive/qt下载`qt-everywhere-opensource-src-4.8.7.tar.gz`

### 1.2创建编译目录

创建编译目录，接下来的工作都在这个目录中进行

```shell
mkdir Qt
```

### 1.3并将源码包拷贝到编译目录并解压

```shell
mv qt-everywhere-opensource-src-4.8.7.tar.gz  Qt/package
cd Qt
tar -xvf package/qt-everywhere-opensource-src-4.8.7.tar.gz    
cd qt-everywhere-opensource-src-4.8.7/
```

### 1.4拷贝并修改交叉工具链配置

```shell
cd mkspecs/
cp linux-arm-gnueabi-g++/  linux-hi3531D-g++ -r
cd linux-hi3531D-g++/
```

编辑 qmake.conf 文件，修改交叉工具链：

```makefile
# modifications to g++.conf
QMAKE_CC                = arm-linux-gnueabi-gcc
QMAKE_CXX               = arm-linux-gnueabi-g++
QMAKE_LINK              = arm-linux-gnueabi-g++
QMAKE_LINK_SHLIB        = arm-linux-gnueabi-g++

# modifications to linux.conf
QMAKE_AR                = arm-linux-gnueabi-ar cqs
QMAKE_OBJCOPY           = arm-linux-gnueabi-objcopy
QMAKE_STRIP             = arm-linux-gnueabi-strip

```

修改为：

```makefile
# modifications to g++.conf
QMAKE_CC                = arm-hisiv600-linux-gcc
QMAKE_CXX               = arm-hisiv600-linux-g++
QMAKE_LINK              = arm-hisiv600-linux-g++
QMAKE_LINK_SHLIB        = arm-hisiv600-linux-g++

# modifications to linux.conf
QMAKE_AR                = arm-hisiv600-linux-ar cqs
QMAKE_OBJCOPY           = arm-hisiv600-linux-objcopy
QMAKE_STRIP             = arm-hisiv600-linux-strip
```

### 1.5配置编译

回到源码顶层目录

```
cd ../../
```

创建编译脚本 `make.sh`，并添加如下内容

```shell
./configure --prefix=../install/hisiv600/Qt4.8.7 \
-opensource \
-confirm-license \
-no-qt3support \
-no-phonon \
-no-phonon-backend \
-no-multimedia \
-no-gtkstyle \
-no-svg \
-no-webkit \
-no-javascript-jit \
-no-script \
-no-scripttools \
-no-declarative \
-no-declarative-debug \
-qt-gfx-linuxfb \
-qt-zlib \
-no-gif \
-qt-libtiff \
-qt-libpng \
-no-libmng \
-qt-libjpeg \
-no-rpath \
-no-pch \
-no-3dnow \
-no-avx \
-no-neon \
-no-openssl \
-no-nis \
-no-cups \
-no-dbus \
-embedded arm \
-xplatform linux-hi3531D-g++ \
-little-endian \
-qt-freetype \
-no-opengl \
-no-glib \
-nomake demos \
-nomake examples \
-nomake docs
```

注意：

>--prefix=../install/hisiv600  是指定安装路径为 ../install/hisiv600   
>-xplatform linux-hi3531D-g++是指定编译工具链配置文件  

创建安装目录，并执行脚本

```shell
./make.sh
```

### 1.6编译安装

```shell
make
make install
```

## 2.编写测试

### 2.1vo

要初始化VO后才能显示，这里使用HDMI输出

 vim ./mpp/component/hdmi/sample/hdmi_mst.c +501

### 2.2库拷贝环境变量设置

临时：

```
mount -t nfs -o nolock 192.168.11.82:/home/work/quanqixian/Qt/  /mnt/
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/mnt/install/hisiv600/Qt4.8.7/lib"
export QT_QWS_FONTDIR=/mnt/install/hisiv600/Qt4.8.7/lib/fonts
```



### 2.3测试程序

```

```

错误

```
QWSSocket::connectToLocalFile could not connect:: No such file or directory   
```

解决

```
https://www.it610.com/article/5262290.htm
```



2.4TODO