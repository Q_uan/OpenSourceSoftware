# C/C++实现的ini解析库-inih

## 一、ini文件的格式

INI文件由3个重要的部分组成:参数(parameters)，段(sections)和注释(comments).其格式如下：

- 段(sections)

```ini
[section]
```

- 参数(parameters)

```ini
name=value
```

- 注释(comments)

```ini
;comments
```

每个段包括段名，注释，和一定的参数对，段名不可重复。段内的参数对是有序的，可重复的。
注释一般以分号“;”开头，在分号后面的文字，直到该行结尾都全部为注解。但是也有很多的配置文件的注释是以“#”开头的

## 二、ini解析库

ini文件解析库有很多，例如以下几种：

1.iniparser

```
https://github.com/ndevilla/iniparser
```

2.libconfig （一个常用的配置文件库，不确定是否支持ini格式的解析）

```
https://github.com/hyperrealm/libconfig
```

3.inifile

```
介绍：https://blog.csdn.net/qq910894904/article/details/38583485
github地址：https://github.com/Winnerhust/inifile2
```

4.inih

```
介绍：http://www.jyguagua.com/?p=3323
github地址：https://github.com/benhoyt/inih
```

## 三、inih的使用

这里稍微了解一下inih的使用，因为它在github上star相对其他ini解析库要多一些。

### 1.介绍

inih是用C语言编写，它被设计得很小，很简单，所以适合嵌入式系统，而且可以不用编译成库，直接包含源文件即可。只实现了ini文件的读取，没有实现写入。

如果想使用c++来解析ini，并且能够接受STL的引入，那么还有一个易于使用的INIReader类，它将键值对存放到map当中。不过c++的API是比较简单的，不完善的，而且作者不计划去继续完善它，可能有些复杂的使用场合不能满足需求。

### 2.c++版ini解析demo

使用inih只需将ini.h、ini.c、INIReader.cpp INIReader.h 这个四个文件提取出来并简单修改下头文件路径即可 。参照官网给出的cpp版本的example，编写一个简单的ini解析测试,example.cpp内容如下：

```c++
#include <iostream>
#include <string>

/* 包含ini解析类头文件 */
#include "INIReader.h"

/* 存放读取结果的结构体 */
typedef struct
{
    std::string HttpServerIp;
    int HttpServerPort;
    bool active;
    double pi;
}InanterConfig;


int tsetINIReader_01(void)
{
    bool ret = true;
    
    INIReader reader("./test.ini");
    if (reader.ParseError() != 0) 
    {
        std::cout << "Can't load 'test.ini'\n";
        return 1;
    }

    /* 检查是否有这个段 */
    ret = ret && reader.HasSection("Http");
    if(false == ret)
    {
        std::cout <<__FILE__<<":"<<__LINE__<<" No this section in config file."<<std::endl;
        return  1;
    }
    
    /* 检查段中是否有这些value */
    ret = ret && reader.HasValue("Http", "HttpServerIp");
    ret = ret && reader.HasValue("Http", "HttpServerPort");
    ret = ret && reader.HasValue("Http", "active");
    ret = ret && reader.HasValue("Http", "pi");
    if(false == ret)
    {
        std::cout <<__FILE__<<":"<<__LINE__<<" Lose items in config file."<<std::endl;
        return  1;
    }
    
    /* 读取 */
    InanterConfig result;

    result.HttpServerIp = reader.Get("Http", "HttpServerIp", "UNKNOWN");    /* 如果没有获取到，则返回设定的默认值：UNKNOWN */
    result.HttpServerPort= reader.GetInteger("Http", "HttpServerPort", -1); /* int型*/
    result.active    = reader.GetBoolean("Http", "active", true);           /* bool型*/
    result.pi        = reader.GetReal("Http", "pi", -1);                    /* 浮点型*/

    /* 打印 */
    std::cout << "Read from config file："<<std::endl;
    std::cout << "HttpServerIp："<<result.HttpServerIp<<std::endl;
    std::cout << "HttpServerPort："<<result.HttpServerPort<<std::endl;
    std::cout << "active："<<result.active<<std::endl;
    std::cout << "pi："<<result.pi<<std::endl;

    return 0;
}

int main()
{
    tsetINIReader_01();

    return 0;
}
```

测试对应的配置文件test.ini内容如下：

```ini
;ini配置文件解析测试
[Http]        ; 
HttpServerIp = 192.168.1.33 ; Test a string 
HttpServerPort = 6666       ; Test a int 
active = true               ; Test a boolean
pi = 3.14159                ; Test a floating point number
```

注意配置文件中的注释开头分号“;”必须与value之间有空格，否则这个库是不能解析的哦。

[完整代码在这里查看](https://gitee.com/Q_uan/OpenSourceSoftware/tree/master/inihDemo/1_INIReaderTest)

