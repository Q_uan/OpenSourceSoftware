#include <iostream>
#include <string>

/* 包含ini解析类头文件 */
#include "INIReader.h"

/* 存放读取结果的结构体 */
typedef struct
{
    std::string HttpServerIp;
    int HttpServerPort;
    bool active;
    double pi;
}InanterConfig;


int tsetINIReader_01(void)
{
    bool ret = true;
    
    INIReader reader("./test.ini");
    if (reader.ParseError() != 0) 
    {
        std::cout << "Can't load 'test.ini'\n";
        return 1;
    }

    /* 检查是否有这个段 */
    ret = ret && reader.HasSection("Http");
    if(false == ret)
    {
        std::cout <<__FILE__<<":"<<__LINE__<<" No this section in config file."<<std::endl;
        return  1;
    }
    
    /* 检查段中是否有这些value */
    ret = ret && reader.HasValue("Http", "HttpServerIp");
    ret = ret && reader.HasValue("Http", "HttpServerPort");
    ret = ret && reader.HasValue("Http", "active");
    ret = ret && reader.HasValue("Http", "pi");
    if(false == ret)
    {
        std::cout <<__FILE__<<":"<<__LINE__<<" Lose items in config file."<<std::endl;
        return  1;
    }
    
    /* 读取 */
    InanterConfig result;

    result.HttpServerIp = reader.Get("Http", "HttpServerIp", "UNKNOWN");    /* 如果没有获取到，则返回设定的默认值：UNKNOWN */
    result.HttpServerPort= reader.GetInteger("Http", "HttpServerPort", -1); /* int型*/
    result.active    = reader.GetBoolean("Http", "active", true);           /* bool型*/
    result.pi        = reader.GetReal("Http", "pi", -1);                    /* 浮点型*/

    /* 打印 */
    std::cout << "Read from config file："<<std::endl;
    std::cout << "HttpServerIp："<<result.HttpServerIp<<std::endl;
    std::cout << "HttpServerPort："<<result.HttpServerPort<<std::endl;
    std::cout << "active："<<result.active<<std::endl;
    std::cout << "pi："<<result.pi<<std::endl;

    return 0;
}

int main()
{
    tsetINIReader_01();

    return 0;
}
