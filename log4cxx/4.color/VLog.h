#pragma once
#include "log4cxx/logger.h"


#define PRINT_RED                    "\033[1;31m"         /* 鲜红 */
#define PRINT_L_PURPLE               "\033[1;35m"         /* 亮粉 */
#define PRINT_YELLOW                 "\033[1;33m"         /* 鲜黄 */
#define PRINT_L_GREEN                "\033[1;32m"         /* 鲜绿 */
#define PRINT_NONE                   "\033[0m"
#define PRINT_COLOR_LOG              (1)                  /* 是否打印有颜色的日志 */

#if PRINT_COLOR_LOG
    #define VLOG_FATAL(logger, fmt, ...)   LOG4CXX_FATAL(logger, PRINT_RED fmt PRINT_NONE, ##__VA_ARGS__)
    #define VLOG_ERROR(logger, fmt, ...)   LOG4CXX_ERROR(logger, PRINT_RED fmt PRINT_NONE, ##__VA_ARGS__)
    #define VLOG_WARN(logger, fmt, ...)    LOG4CXX_WARN(logger,  PRINT_L_PURPLE fmt PRINT_NONE, ##__VA_ARGS__)
    #define VLOG_INFO(logger, fmt, ...)    LOG4CXX_INFO(logger,  PRINT_L_GREEN fmt PRINT_NONE, ##__VA_ARGS__)
    #define VLOG_DEBUG   LOG4CXX_DEBUG
    #define VLOG_TRACE   LOG4CXX_TRACE
#else
    #define VLOG_FATAL   LOG4CXX_FATAL
    #define VLOG_ERROR   LOG4CXX_ERROR
    #define VLOG_WARN    LOG4CXX_WARN
    #define VLOG_INFO    LOG4CXX_INFO
    #define VLOG_DEBUG   LOG4CXX_DEBUG
    #define VLOG_TRACE   LOG4CXX_TRACE
#endif
