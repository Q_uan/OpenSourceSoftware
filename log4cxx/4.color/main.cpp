#include "log4cxx/logger.h"
#include "log4cxx/basicconfigurator.h"
#include "log4cxx/propertyconfigurator.h"
#include "log4cxx/logmanager.h"
#include "VLog.h"
 
static auto logger = log4cxx::Logger::getLogger("MyApp");
 
void foo() {
    // Get a logger that is a child of the statically declared logger
    auto fooLogger = log4cxx::Logger::getLogger("MyApp.foo");
    VLOG_TRACE(fooLogger, "Doing foo at trace level");
    VLOG_DEBUG(fooLogger, "Doing foo at debug level");
    VLOG_INFO(fooLogger, "Doing foo at info level");
    VLOG_WARN(fooLogger, "Doing foo at warn level");
    VLOG_ERROR(fooLogger, "Doing foo at error level");
    VLOG_FATAL(fooLogger, "Doing foo at fatal level");
}
 
int main(int argc, char **argv) {
	std::string filePath = "./log4cxx.property";
	if (log4cxx::PropertyConfigurator::configure(filePath) == 
			log4cxx::spi::ConfigurationStatus::NotConfigured) {
		log4cxx::BasicConfigurator::configure();
	}

	foo();

	log4cxx::LogManager::shutdown();

    return 0;
}
