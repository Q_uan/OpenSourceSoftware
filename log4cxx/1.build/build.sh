#!/bin/bash 
CUR_PATH=`pwd`
INSTALL_PATH=$CUR_PATH/install/ #安装目录

#依赖库安装到临时安装目录，只需要它们的静态库
INSTALL_TMP_PATH=$CUR_PATH/install_tmp
APR_INSTALL_PATH=$INSTALL_TMP_PATH
APR_UTIL_INSTALL_PATH=$INSTALL_TMP_PATH

#############################
#func  :编译apr
#############################
function build_apr()
{
	# wget https://archive.apache.org/dist/apr/apr-1.7.4.tar.bz2
	mkdir -p $APR_INSTALL_PATH
	tar -xvf apr-1.7.4.tar.bz2
	cd apr-1.7.4
	CFLAGS=-fPIC ./configure --prefix=$APR_INSTALL_PATH
	make 
	make install
	cd -
}

#############################
#func  :编译aprutil
#############################
function build_apr_util()
{
	# wget https://archive.apache.org/dist/apr/apr-util-1.6.3.tar.gz
	mkdir -p $APR_UTIL_INSTALL_PATH
	tar -xvf apr-util-1.6.3.tar.gz
	cd apr-util-1.6.3
	CFLAGS=-fPIC ./configure --with-apr=$APR_INSTALL_PATH --prefix=$APR_INSTALL_PATH
	make 
	make install
	cd -
}

#############################
#func  :编译log4cxx
#############################
function build_log4cxx()
{
	#wget https://dlcdn.apache.org/logging/log4cxx/1.4.0/apache-log4cxx-1.4.0.tar.gz  
	mkdir -p $INSTALL_PATH
	tar -xvf apache-log4cxx-1.4.0.tar.gz 
	cd apache-log4cxx-1.4.0/
	mkdir build
	cmake -S . -B build -DAPR_STATIC=yes -DAPU_STATIC=yes \
	-DCMAKE_PREFIX_PATH=$CUR_PATH/install_tmp \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_PATH  \
	-DCMAKE_BUILD_TYPE=Release

	cmake --build build/ --target install
}


#############################
#func  :清除编译
#############################
function clean()
{
	rm -rf apr-1.7.4
	rm -rf apr-util-1.6.3
	rm -rf apache-log4cxx-1.4.0
	rm -rf $INSTALL_PATH
	rm -rf $INSTALL_TMP_PATH
	echo "clean finished"
}

function buildAll()
{
	build_apr
	build_apr_util
	build_log4cxx
	echo "build finished"
}

#########################
#func: 菜单
#########################
function menu()
{

	if [ $# -ne 1 ]
	then
		echo "usage : $0 build/clean"
		exit
	fi

	if   [ $1 = "build" ]
	then
		buildAll
	elif [ $1 = "clean" ]
	then
		clean
	else
		echo "usage : $0 build/clean"
	fi
}


function main()
{
	menu $*
}
main $*
