#include <iostream>
#include <fstream>
#include "Message.pb.h"

void ListMsg(const Test::Person & msg)
{
    std::cout << msg.name() << std::endl;
    std::cout << msg.age() << std::endl;
    std::cout << msg.occupation() << std::endl;
}

int main()
{
    Test::Person msg;
    std::fstream input("./log", std::ios::in|std::ios::binary);

    if(!msg.ParseFromIstream(&input))
    {
        std::cerr << "Failed to parse address book." << std::endl;
        return -1;
    }
    ListMsg(msg);

    return 0;
}

