#include <iostream>
#include <fstream>
#include "Message.pb.h"

int main()
{
    Test::Person msg;

    msg.set_name("Takayama");
    msg.set_age(27);
    msg.set_occupation("idol");

    std::fstream output("./log", std::ios::out|std::ios::trunc|std::ios::binary);

    if(!msg.SerializeToOstream(&output))
    {
        std::cerr << "Failed to write msg." << std::endl;
        return -1;
    }

    return 0;
}
