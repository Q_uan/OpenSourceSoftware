#!/bin/bash 
#这个脚本用于自动生成README.md文件


OUT_FILE="./README.md"

cat /dev/null >$OUT_FILE

echo "这个目录中存放了开源软件的使用demo">>$OUT_FILE
echo "">>$OUT_FILE
echo "有些目录只有代码没有markdown文件">>$OUT_FILE
echo "">>$OUT_FILE
echo "这里列出了目录中的markdown文件:">>$OUT_FILE
echo "">>$OUT_FILE

#查找所有markdown文件
markdown_files=`find ./ -name "*.md" |sort -g`

#将markdown笔记路径按照markdown文件格式写入文件
for i in $markdown_files
do
    base_name=`basename $i`
    echo "-   [$base_name]($i)" >>$OUT_FILE
    echo "">>$OUT_FILE
done
