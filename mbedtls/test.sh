#! /bin/bash

set -xe

DEMO_NAME=a.demo.out
KEY_PRIV=private.pem
KEY_PUB=public.pem
RAW_FILE=demo_verify_sign.c
SIGN_FILE=demo.sig


# Build Demo
[[ ! -e ./mbedtls ]] && make clone
make build BIN=${DEMO_NAME} -j &


# Gen Private Key
openssl genrsa -out ${KEY_PRIV}
# Gen Public Key
openssl rsa -in ${KEY_PRIV} -out ${KEY_PUB} -pubout


# Sign (Using openssl)
openssl dgst -sha256 -sign ${KEY_PRIV} -out ${SIGN_FILE} ${RAW_FILE}

# Verify (Using openssl)
openssl dgst -prverify ${KEY_PRIV} -sha256 -signature  ${SIGN_FILE} ${RAW_FILE}

# Verify (Using Demo)
echo "Wait for demo compilation to complete .."
wait
./${DEMO_NAME} ${KEY_PUB} ${RAW_FILE} ${SIGN_FILE}
