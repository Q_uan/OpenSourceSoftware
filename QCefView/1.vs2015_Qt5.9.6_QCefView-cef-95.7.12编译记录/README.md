# 1.编译环境

visual Studio版本：Visual Studio 2015

qt版本：Qt5.9.6

QCefView版本：cef-95.7.12

# 2.下载所需文件

## 2.1下载QCefView的cef-95.7.12版本

进入https://github.com/CefView/QCefView点击 tags按钮，选择cef-95.7.12版本下载，得到QCefView-cef-95.7.12.zip

## 2.2下载CefViewCore的cef-95.7.12版本

QCefView工程里有CefViewCore目录，但是是空的，在用CMake配置项目时，会下载CefViewCore， 也可以手动下载, 放到指定目录即可。

进入https://github.com/CefView/CefViewCore点击 tags按钮，选择cef-95.7.12版本下载，得到CefViewCore-cef-95.7.12.zip

## 2.3下载Cef的cef-95.7.12版本

在用CMake配置项目时，会下载cef， 也可以手动下载, 放到指定目录即可。

参考QCefView官方手册中https://cefview.github.io/QCefView/docs/intros/change-cef-ver，进入https://cef-builds.spotifycdn.com/index.html 下载所需cef版本。这里下载cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32.tar.bz2 

![](./pic/cef_binary.jpg)

# 3.生成VS解决方案

## 3.1解压文件

- 解压QCefView-cef-95.7.12.zip到QCefView-cef-95.7.12目录
- 解压CefViewCore-cef-95.7.12.zip到CefViewCore-cef-95.7.12目录
- 将CefViewCore-cef-95.7.12目录下的内容拷贝到QCefView-cef-95.7.12/CefViewCore
- 将 cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32.tar.bz2 拷贝到QCefView-cef-95.7.12/CefViewCore/dep目录，无需解压，可以查看

QCefView-cef-95.7.12\CefViewCore\CefConfig.cmake中的CEF_SDK_VERSION变量确认正确的cef版本或者修改为其他版本：

```cmake
#
# The link for downloading the CEF binary sdk 
#
set(CEF_SDK_VERSION 
  # Old version (deprecated and incompatible)
  # "89.0.12+g2b76680+chromium-89.0.4389.90"

  # Current version
  "95.7.12+g99c4ac0+chromium-95.0.4638.54"

  # Newer version (need to adpat)
  # --
)
```

由于此版本默认没有32位支持，需要修改QCefView-cef-95.7.12\CefViewCore\CefConfig.cmake中的CEF_SDK_PLATFORM值

```
if(OS_WINDOWS)
  set(CEF_SDK_PLATFORM "windows64")

改为：

if(OS_WINDOWS)
  set(CEF_SDK_PLATFORM "windows32")

```



## 3.2使用cmake生成vs工程

```
 cmake -S . -B build/windows.x86 -DBUILD_DEMO=ON -DPROJECT_ARCH=x86 -DCMAKE_BUILD_TYPE=Release
```

# 4.使用vs2015编译

- 使用vs2015打开QCefView-cef-95.7.12\build\windows.x86\QCefView.sln
- 选择Release/Win32
- 选择ALL_BUILD执行生成

## 4.1编译问题-01

```
严重性	代码	说明	项目	文件	行
错误	C2057	应输入常量表达式 (编译源文件 E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\libcef_dll\base\cef_weak_ptr.cc)	libcef_dll_wrapper	E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\include\base\cef_scoped_refptr.h	257
```

对应代码：（QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\include\base\cef_scoped_refptr.h）

```c++
  ~scoped_refptr() {
    static_assert(!base::cef_subtle::IsRefCountPreferenceOverridden(
                      static_cast<T*>(nullptr), static_cast<T*>(nullptr)),
                  "It's unsafe to override the ref count preference."
                  " Please remove REQUIRE_ADOPTION_FOR_REFCOUNTED_TYPE"
                  " from subclasses.");
    if (ptr_)
      Release(ptr_);
  }
```

暂将此静态断言注销掉

```c++
  ~scoped_refptr() {
#if 0
    static_assert(!base::cef_subtle::IsRefCountPreferenceOverridden(
                      static_cast<T*>(nullptr), static_cast<T*>(nullptr)),
                  "It's unsafe to override the ref count preference."
                  " Please remove REQUIRE_ADOPTION_FOR_REFCOUNTED_TYPE"
                  " from subclasses.");
#endif
    if (ptr_)
      Release(ptr_);
  }
```

## 4.2编译问题-02

```cmake
严重性	代码	说明	项目	文件	行
错误	C2338	Receivers may not be raw pointers. If using a raw pointer here is safe and has no lifetime concerns, use base::Unretained() and document why it's safe. (编译源文件 E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\libcef_dll\wrapper\cef_message_router.cc)	libcef_dll_wrapper	E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\include\base\internal\cef_bind_internal.h	978
```

对应代码：（QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\include\base\internal\cef_bind_internal.h+974）

```c++
  static_assert(
      !std::is_pointer<DecayedReceiver>::value ||
          IsRefCountedType<std::remove_pointer_t<DecayedReceiver>>::value,
      "Receivers may not be raw pointers. If using a raw pointer here is safe"
      " and has no lifetime concerns, use base::Unretained() and document why"
      " it's safe.");
```

暂将此静态断言注销掉

```
#if 0
  static_assert(
      !std::is_pointer<DecayedReceiver>::value ||
          IsRefCountedType<std::remove_pointer_t<DecayedReceiver>>::value,
      "Receivers may not be raw pointers. If using a raw pointer here is safe"
      " and has no lifetime concerns, use base::Unretained() and document why"
      " it's safe.");
#endif
```

## 4.3编译问题-03

```
严重性	代码	说明	项目	文件	行
错误	C2220	警告被视为错误 - 没有生成“object”文件 (编译源文件 E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\libcef_dll\wrapper\cef_resource_manager.cc)	libcef_dll_wrapper	E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\dep\cef_binary_95.7.12+g99c4ac0+chromium-95.0.4638.54_windows32\include\base\internal\cef_bind_internal.h	202

```

将警告视为错误改为否，如下图所示：

![err03_solve](./pic/err03_solve.jpg)

## 4.4编译问题-04

```
严重性	代码	说明	项目	文件	行
错误	C2039	“tolower”: 不是“std”的成员	CefViewCore	E:\client\CefSum\QCefView-cef-95.7.12\CefViewCore\src\CefView\CefBrowserApp\CefViewBrowserClient.cpp	53

```

不使用std下的tolower即可，在文件QCefView-cef-95.7.12\CefViewCore\src\CefView\CefBrowserApp\CefViewBrowserClient.cpp中增加头文件：

```
#include <ctype.h>
```

```
lower_url.begin(), lower_url.end(), lower_url.begin(), [](unsigned char c) { return std::tolower(c); });
改为:
lower_url.begin(), lower_url.end(), lower_url.begin(), [](unsigned char c) { return tolower(c); });
```

## 4.5编译问题-05

```
严重性	代码	说明	项目	文件	行
错误	C1083	无法打开包括文件: “QRandomGenerator”: No such file or directory	QCefViewTest	E:\client\CefSum\QCefView-cef-95.7.12\example\QCefViewTest\CefViewWidget.cpp	2
```

由于qt版本过低，没有QRandomGenerator实现，而且相关调用仅在示例QCefViewTest中，注销掉头文件，稍作修改即可。

```
 QColor color(QRandomGenerator::global()->generate());
 改为
  QColor color(127，127，127);
```

