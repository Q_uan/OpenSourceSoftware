# 1.lrzsz介绍

​	rz，sz是Linux/Unix同Windows进行ZModem文件传输的命令行工具。windows端需要支持ZModem的telnet/ssh客户端（比如SecureCRT）。

​	优点就是不用再开一个sftp工具登录上去上传下载文件。运行命令rz、sz要比FTP容易很多，而且服务器不需要另开FTP服务即可完成。

- sz：将选定的文件发送（send）到本地机器。
- rz：运行该命令会弹出一个文件选择窗口，从本地选择文件上传到Linux服务器。

官方网址：

```
https://www.ohse.de/uwe/software/lrzsz.html
```

github上的代码仓库（非官方）

```shell
https://github.com/jnavila/lrzsz
```

# 2.lrzsz安装

## 1.ubuntu和centos下的软件包安装命令

centos下：

```shell
yum install lrzsz
```

ubuntu下：

```shell
sudo apt-get install lrzsz
```

## 2.源码方式安装

使用wget下载源码包：

```shell
wget https://www.ohse.de/uwe/releases/lrzsz-0.12.20.tar.gz

```

解压：

```shell
 tar -xzvf lrzsz*.tar.gz
```

编译安装：

```shell
./configure
make
make install
```

默认安装在了 了/usr/local/bin/ 目录, 需要在/usr/bin目录创建软链接:

```shell
ln -s /usr/local/bin/lrz /usr/bin/rz
ln -s /usr/local/bin/lsz /usr/bin/sz
```

修改权限

```shell
chmod 777 /usr/local/bin/lrz
chmod 777 /usr/local/bin/lsz
```

