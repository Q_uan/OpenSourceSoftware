## **1.编译和安装**

下载源码

```shell
git clone https://github.com/google/googletest.git
```

进入源码目录

```shell
cd googletest/
```

创建编译目录`build`  和安装目录`install`

```shell
mkdir build  install
```

编译安装

​	不安装到系统默认目录，安装到自己指定的目录`../install`中

```shell
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install ..
make
make install
```

## 2.编写简单的测试

编写gtest.cpp如下：

```c++
#include <gtest/gtest.h>

int foobar(void)
{
    return 1;
}

TEST(foobarbar, test)
{
    ASSERT_EQ(1, foobar());
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
```

编写Makefile文件并指定从googletest的安装路径中寻找头文件和库文件

```makefile
test:gtest.cpp
	g++ $^ -o $@ \  
	        ./googletest/install/lib64/libgtest.a  \
	        -I./googletest/install/include/ \
	        -lpthread   \
	        -std=c++11   
.PHONY:clean
	clean:
	rm -rf gtest *.o
```

运行结果：

```shell
$ ./test 
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from foobarbar
[ RUN      ] foobarbar.test
[       OK ] foobarbar.test (0 ms)
[----------] 1 test from foobarbar (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.
```

