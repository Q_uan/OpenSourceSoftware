-Wno-deprecated-declarations
Do not warn about uses of functions, variables, and types marked as deprecated by using the deprecated attribute. (see Function Attributes, see Variable Attributes, see Type Attributes.)
-Wpacked
from https://gcc.gnu.org/onlinedocs/gcc-3.4.6/gcc/Warning-Options.html