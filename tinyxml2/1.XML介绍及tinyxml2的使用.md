# 1. XML介绍

XML 指可扩展标记语言（e**X**tensible **M**arkup **L**anguage）。

XML文件格式是纯文本格式，在许多方面类似于HTML，XML由XML元素组成，每个XML元素包括一个开始标记，一个结束标记以及两个标记之间的内容。

例如：

```xml
<Name>Tom</Name>
```

## 1.1 XML标签

在 HTML，经常会看到没有关闭标签的元素：

```html
<p>This is a paragraph
```

在 XML 中，省略关闭标签是非法的。所有元素都*必须*有关闭标签：

```xml
<p>This is a paragraph</p>
```

## 1.2 XML声明

下面的例子中，第一行是 XML 声明。它定义 XML 的版本 (1.0) 和所使用的编码 (ISO-8859-1 = Latin-1/西欧字符集)

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<note>
    <to>Akimoto</to>
    <from>Asuka</from>
    <heading>Reminder</heading>
    <body>Don't forget the meeting!</body>
</note>
```

## 1.3 XML属性

在 HTML 中，属性提供有关元素的额外信息。属性通常提供不属于数据组成部分的信息。

在下面的实例中，文件类型与数据无关，但是对需要处理这个元素的软件来说却很重要：

```xml
<file type="gif">computer.gif</file>
```

**XML 属性必须加引号，属性值必须被引号包围，不过单引号和双引号均可使用**。比如一个人的性别，person 元素可以这样写：

```xml
<person sex="female">
```

或者这样也可以：

```xml
<person sex='female'>
```

如果属性值本身包含双引号，您可以使用单引号，就像这个实例：

```xml
<gangster name='George "Shotgun" Ziegler'>
```

或者您可以使用字符实体：

```xml
<gangster name="George &quot;Shotgun&quot; Ziegler">
```

## 1.4 XML注释

在 XML 中编写注释的语法与 HTML 的语法很相似。

```xml
<!-- This is a comment -->
```

## 1.5 XML在线格式化

https://c.runoob.com/front-end/710/

# 2. tinyxml2

## 2.1 tinyxml2介绍

TinyXML-2 is a simple, small, efficient, C++ XML parser that can be easily integrated into other programs.

- The master is hosted on github: https://github.com/leethomason/tinyxml2

- The online HTML version of these docs: http://leethomason.github.io/tinyxml2/


## 2.2 tinyxml 和tinyxml2

### TinyXML-1 vs. TinyXML-2

> TinyXML-2 long been the focus of all development. It is well tested and should be used instead of TinyXML-1.
>
> TinyXML-2 uses a similar API to TinyXML-1 and the same rich test cases. But the implementation of the parser is completely re-written to make it more appropriate for use in a game. It uses less memory, is faster, and uses far fewer memory allocations.
>
> TinyXML-2 has no requirement or support for STL.

### TinyXML-1 or TinyXML-2?

> Both parsers:
>
> 1. Simple to use with similar APIs.
> 2. DOM based parser.
> 3. UTF-8 Unicode support. http://en.wikipedia.org/wiki/UTF-8
>
> Advantages of TinyXML-2
>
> 1. The focus of all future dev.
> 2. Many fewer memory allocation (1/10th to 1/100th), uses less memory (about 40% of TinyXML-1), and faster (~5x on read).
> 3. No STL requirement.
> 4. More modern C++, including a proper namespace.
> 5. Proper and useful handling of whitespace
>
> Advantages of TinyXML-1
>
> 1. Can report the location of parsing errors.
> 2. Support for some C++ STL conventions: streams and strings
> 3. Very mature and well debugged code base.

摘自：http://www.grinninglizard.com/tinyxml2/

## 2.3 添加tinyxml2 到项目中

只需要将`tinyxml2.cpp`和`tinyxml2.h`拷贝至项目目录，使用时包含头文件，声明命名空间即可

```c++
#include "tinyxml2.h" 
using namespace tinyxml2;
```

## 2.4 官方编程示例

### 官方示例1-加载XML文件

```c++
int example_1()
{
    int xmlRet = 0;
    XMLDocument doc;

    /* 从文件加载xml */
    xmlRet = doc.LoadFile("./in.xml");
    if(tinyxml2::XML_SUCCESS != xmlRet)
    {
        std::cout<<"Fail to load file, ErrorID:"<<doc.ErrorID()<<std::endl;
    }

    /* 将xml保存到文件 */
    xmlRet = doc.SaveFile("out.xml");
    if(tinyxml2::XML_SUCCESS != xmlRet)
    {
        std::cout<<"Fail to save file, ErrorID:"<<doc.ErrorID()<<std::endl;
    }

    return doc.ErrorID();
}
```

### 官方示例2-从字节内存中解析XML

```c++
int example_2()
{
    static const char* xml = "<element/>";
    XMLDocument doc;
    doc.Parse( xml );

    return doc.ErrorID();
}
```

### 官方示例3-读取XML元素信息

```c++
int example_3()
{
    static const char* xml =
        "<?xml version=\"1.0\"?>"
        "<!DOCTYPE PLAY SYSTEM \"play.dtd\">"
        "<PLAY>"
        "<TITLE>A Midsummer Night's Dream</TITLE>"
        "</PLAY>";

    XMLDocument doc;
    doc.Parse( xml );

    XMLElement* titleElement = doc.FirstChildElement( "PLAY" )->FirstChildElement( "TITLE" );
    const char* title = titleElement->GetText();
    printf( "Name of play (1): %s\n", title );

    XMLText* textNode = titleElement->FirstChild()->ToText();
    title = textNode->Value();
    printf( "Name of play (2): %s\n", title );

    return doc.ErrorID();
}
```

### 官方示例4-读取XML元素属性

```c++
bool example_4()
{
    static const char* xml =
        "<information>"
        "    <attributeApproach v='2' />"
        "    <textApproach>"
        "        <v>2</v>"
        "    </textApproach>"
        "</information>";

    XMLDocument doc;
    doc.Parse( xml );

    int v0 = 0;
    int v1 = 0;

    XMLElement* attributeApproachElement = doc.FirstChildElement()->FirstChildElement( "attributeApproach" );
    attributeApproachElement->QueryIntAttribute( "v", &v0 );

    XMLElement* textApproachElement = doc.FirstChildElement()->FirstChildElement( "textApproach" );
    textApproachElement->FirstChildElement( "v" )->QueryIntText( &v1 );

    printf( "Both values are the same: %d and %d\n", v0, v1 );

    return !doc.Error() && ( v0 == v1 );
}
```

## 2.5 综合示例

例如有下面这样一个xml：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Student sex="male">
    <Name>Tom</Name>
    <Age>8</Age>
    <From>China</From>
</Student>
```

使用tinyxml2创建xml对象，并测试xml和string互转，xml元素，属性获取：

```c++
int main(int argc, const char * argv[])
{
    /**
     * 创建xml
     */
    tinyxml2::XMLDocument doc;
    /* default:<?xml version="1.0" encoding="UTF-8"?> */
    tinyxml2::XMLDeclaration *declaration = doc.NewDeclaration();
    doc.InsertFirstChild(declaration);

    tinyxml2::XMLElement *Student = doc.NewElement("Student");
    Student->SetAttribute("sex", "male");
    doc.InsertEndChild(Student);

    tinyxml2::XMLElement *Name = doc.NewElement("Name");
    Name->InsertNewText("Tom");
    Student->InsertEndChild(Name);

    tinyxml2::XMLElement *Age = doc.NewElement("Age");
    Age->InsertNewText("8");
    Student->InsertEndChild(Age);

    tinyxml2::XMLElement *From = doc.NewElement("From");
    From->InsertNewText("China");
    Student->InsertEndChild(From);

    /**
     * xml转string
     */
    tinyxml2::XMLPrinter printer;
    doc.Print( &printer);

    std::string xmlStr = printer.CStr();
    std::cout<<"createxml:"<<std::endl<<xmlStr<<std::endl;

    /**
     * string转xml
     */
    tinyxml2::XMLDocument outDoc;
    int xmlRet = outDoc.Parse(xmlStr.c_str());
    if(tinyxml2::XML_SUCCESS != xmlRet)
    {
        std::cout<<"Fail to Parse xml, ErrorID:"<<outDoc.ErrorID()<<"str:"<<xmlStr<<std::endl;
        return outDoc.ErrorID();
    }

    /**
     * 解析xml元素和属性
     */
    {
        /* 内容校验 */
        bool ret = true;
        ret = ret && outDoc.FirstChildElement("Student");
        ret = ret && outDoc.FirstChildElement("Student")->FirstChildElement("Name");
        ret = ret && outDoc.FirstChildElement("Student")->FirstChildElement("Age");
        ret = ret && outDoc.FirstChildElement("Student")->FirstChildElement("From");
        if(!ret)
        {
            std::cout<<"Xml parse err,xml:"<<xmlStr<<std::endl;
            return outDoc.ErrorID();
        }
        
        const char* pSex = NULL;
        outDoc.FirstChildElement("Student")->QueryStringAttribute("sex", &pSex);
        std::string name = outDoc.FirstChildElement("Student")->FirstChildElement("Name")->GetText();
        std::string age = outDoc.FirstChildElement("Student")->FirstChildElement("Age")->GetText();
        std::string from = outDoc.FirstChildElement("Student")->FirstChildElement("From")->GetText();

        std::cout<<"sex:"<<pSex<<std::endl;
        std::cout<<"name:"<<name<<std::endl;
        std::cout<<"age:"<<age<<std::endl;
        std::cout<<"from:"<<from<<std::endl;
    }

    return 0;
}
```

## 2.6内存模型

XMLDocument 是一个 C++ 对象，与任何其他对象一样，可以在堆栈上，也可以在堆上新建和删除。

但是，Document、XMLElement、XMLText 等的任何子节点只能通过调用相应的 XMLDocument::NewElement、NewText 等方法来创建。

尽管您有指针指向这些对象，但它们仍归 Document 拥有。**当 Document 被删除时，它所包含的所有节点也会被删除**。

