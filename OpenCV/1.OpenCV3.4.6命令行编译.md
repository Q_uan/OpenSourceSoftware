# 0.说明

- 本次编译使用命令行的方式配置cmake选项，没有使用cmkae-gui
- 本次编译没有使用x264 ffmpeg依赖库等

# 1.编译安装

下载OpenCV源码，此次使用的版本未`3.4.6`，下载完成后得到`opencv-3.4.6.zip`, 下载地址：

```
https://gitee.com/mirrors/opencv/tags
```

解压并进入解压后的目录

```shell
unzip opencv-3.4.6.zip 
cd opencv-3.4.6/
```

创建编译和安装目录

```shell
mkdir install/ build/
cd build/
```

在build目录下创建编译脚本`build.sh`,内容如下：

```shell
#!/bin/bash

cmake -DCMAKE_SYSTEM_NAME="Linux" -DWITH_IPP=OFF -DCMAKE_CXX_COMPILER=/bin/g++ -DCMAKE_C_COMPILER=/bin/gcc -DCMAKE_AR=/bin/ar -DCMAKE_LINKER=/bin/ld  -DCMAKE_NM=/bin/nm -DCMAKE_OBJCOPY=/bin/objcopy -DCMAKE_OBJDUMP=/bin/objdump -DCMAKE_RANLIB=/bin/ranlib -DENABLE_VFPV4=OFF -DBUILD_DOCS=OFF -DBUILD_FAT_JAVA_LIB=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_PROTOBUF=OFF -DWITH_CUDA=OFF -DCMAKE_SHARED_LINKER_FLAGS="-shared" -DWITH_EIGEN=OFF -DWITH_FFMPEG=OFF -DWITH_ITT=OFF -Dopencv_dnn_BUILD_TORCH_IMPORTER=OFF -DOPENCL_FOUND=OFF -DWITH_1394=OFF -DWITH_CAROTENE=OFF -DWITH_CUFFT=OFF -DWITH_GPHOTO2=OFF -DWITH_GSTREAMER=OFF -DWITH_GTK=OFF -DWITH_LAPACK=OFF -DWITH_MATLAB=OFF -DWITH_OPENCL=OFF -DWITH_OPENCLAMDBLAS=OFF -DWITH_OPENCLAMDFFT=OFF -DWITH_OPENEXR=OFF -DBUILD_JASPER=ON -DBUILD_JPEG=ON -DBUILD_PNG=ON -DBUILD_ZLIB=ON -DCMAKE_BUILD_TYPE=Release -DBUILD_STATIC_LIBS -DBUILD_SHARED_LIBS=ON -DWITH_PNG=ON -DWITH_JPEG=ON -DWITH_ZLIB=ON -DWITH_TIFF=OFF -DWITH_WEBP=OFF -DBUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX=../install ..

make
make install
```

执行脚本完成编译安装

# 2.编写简单测试

进入上面创建的安装目录下，并创建test目录用于存放测试

```shell
cd ../install/
mkdir test
```

在test下创建以下文件(test.jpg为测试用图片)

```
`-- test
    |-- main.cpp
    |-- Makefile
    `-- test.jpg
```

main.cpp

```c++
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main()
{
    Mat img = imread("test.jpg");

    /* draw a line */
    line(img, Point(0, img.size().height / 2), Point(img.size().width, img.size().height / 2), Scalar(0, 0, 255));
    /* draw a rectangle */
    rectangle(img, Point(img.size().width/4, img.size().height/4), Point(img.size().width- img.size().width / 4, img.size().height - img.size().height/4), Scalar(0, 255, 255));
    /* draw a circle */
    circle(img, Point(img.size().width / 2, img.size().height / 2), 300, Scalar(255, 0, 255));

    imwrite("afterdraw.jpg", img);
}
```

 Makefile 

```c++
TARGET=test
OUTPUTDIR=./output

$(TARGET): main.cpp
	g++ $^ -o $@ \
            -I../include \
            -L../lib  \
            -lpthread -g \
            -lopencv_imgproc \
            -lopencv_core \
            -lopencv_imgcodecs \
            -Wl,-rpath=./:../lib


.PHONY:clean
clean:
	rm -rf $(TARGET)
```

编译代码，执行可执行文件，将在当前目录下生成处理后的图片文件

```shell
make
./test
```

