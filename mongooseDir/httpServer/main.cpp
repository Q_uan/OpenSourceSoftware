#include <iostream>
#include <memory>
#include "HttpServer.h"


bool handle_fun2(std::string url, std::string body, void* arg)
{
    std::cout << "handle fun2" << std::endl;
    std::cout << "url: " << url << std::endl;
    std::cout << "body: " << body << std::endl;
    std::cout << "arg : " << ( const char *)arg << std::endl;

    return true;
}

int main(int argc, char *argv[]) 
{
    HttpServer http_server2;
    const char * str = "args";

    http_server2.Init("80");
    http_server2.Start();
    http_server2.AddHandler("/api/fun2", handle_fun2, (void*)str);


    std::cout<<"......"<<std::endl;
    while(1)
    {
        sleep(10);
    }

    return 0;
}
