# 1.明确依赖库

在ActiveMq官网[Building ActiveMQ-CPP](https://activemq.apache.org/components/cms/developers/building)页面给出了编译流程和部分依赖库的下载链接，并且可以知道编译ActiveMq-CPP依赖下面这些库：

| 库      | 包含子项                                         |
| ------- | ------------------------------------------------ |
| libuuid |                                                  |
| CppUnit |                                                  |
| APR     | APR 1.7.0<br/>APR-util 1.6.1<br/>APR-iconv 1.2.2 |
| openssl |                                                  |

# 2.分步编译

此次所有库的编译都在当前目录为ActiveMQ-CppBuild的目录下进行，并且所有库的安装路径都是当前目录下的build目录：

```shell
├── ActiveMQ-CppBuild
│   ├── activemq-cpp-library-3.9.5-src.tar.bz2
│   ├── apr-1.7.0.tar.gz
│   ├── apr-iconv-1.2.2.tar.gz
│   ├── apr-util-1.6.1.tar.gz
│   ├── build.sh
│   ├── cppunit-1.12.1.tar.gz
│   ├── libuuid-1.0.3.tar.gz
│   └── openssl-1.1.1n.tar.gz
```

设置安装目录环境变量：

```shell
CUR_PATH=`pwd`
INSTALL_PATH=$CUR_PATH/build/ #安装目录

LIBUUID_INSTALL=$INSTALL_PATH
CPPUNIT_INSTALL=$INSTALL_PATH
APR_INSTALL=$INSTALL_PATH
APR_UTIL_INSTALL=$APR_INSTALL
APR_ICONV_INSTALL=$APR_INSTALL
OPENSSL_INSTALL=$INSTALL_PATH
ACTIVEMQ_CPP_INSTALL=$INSTALL_PATH
```



## 2.1安装libuuid

libuuid被APR库和activemq-cpp编译时依赖，并且在编译时会到系统路径中查找，这里按照ActiveMq官网[Building ActiveMQ-CPP](https://activemq.apache.org/components/cms/developers/building)页面给出了编译流程，选择直接在系统中安装libuuid。

在编译APR时，如果想设置到自定义的编译路径查找自己源码编译出的libuuid比较麻烦， 所以这里按照activemq-cpp官方提示安装libuuid到系统中，避免不必要的麻烦。

之后使用库时，可以将系统中的libuuid拷贝到编译工程路径，使编译结果在当前目录下查找库，避免对系统的依赖。



- [x] 注意：经测试，即使不使用libuuid，也可以编译通过所有库及测试。在编译自己的代码时不使用-luuid即可。

### 2.1.1使用命令安装

在Ubuntu下可以使用以下命令安装：

```
sudo apt-get install uuid-dev
```

### 2.1.2源码编译安装

当系统没有安装libuuid时，可以选择源码的安装方式：

下载libuuid，在下面的网页下载：

```shell
https://sourceforge.net/projects/libuuid/ 
```

编译步骤：

```shell
function buildLibUUID()
{
    mkdir -p $LIBUUID_INSTALL #未使用
    tar -xvf  libuuid-1.0.3.tar.gz
    cd libuuid-1.0.3
    ./configure  #--prefix=$LIBUUID_INSTALL
    make -j
    make install
    cd ..
}
```

- [x] 注意：这里没有设置安装路径而是采用默认安装路径。

## 2.2编译cppunit

下载地址：

```shell
https://sourceforge.net/projects/cppunit/files/cppunit/1.12.1/cppunit-1.12.1.tar.gz/download
```

编译：

```shell
function buildCppUnit()
{
    mkdir -p $CPPUNIT_INSTALL
    tar -xvf  cppunit-1.12.1.tar.gz
    cd cppunit-1.12.1 

    ./configure  --prefix=$CPPUNIT_INSTALL
    make -j
    make install
    cd ..
}
```

- [x] 注意：因为Activemq需要使用到cppunit-config工具，而当前最新版本（cppunit-1.15）编译成果中没有这个可执行物，所以选择cppunit-1.12.1版本，而没有使用[官网](https://www.freedesktop.org/wiki/Software/cppunit/)最新版本。


## 2.3编译APR

使用 [Apache Portable Runtime](http://apr.apache.org/) 界面给出的版本：APR 1.7.0

下载地址：

```
https://apr.apache.org/download.cgi
```

编译：

```shell
function buildApr()
{
    mkdir -p $APR_INSTALL
    tar -xvf ./apr-1.7.0.tar.gz 
    cd apr-1.7.0

    ./configure  --prefix=$APR_INSTALL
    make -j
    make install
    cd ..
}
```

## 2.4编译APR-util 

使用 [Apache Portable Runtime](http://apr.apache.org/) 界面给出的版本：APR-util 1.6.1<br/>

下载地址：

```
https://apr.apache.org/download.cgi
```

编译：

```shell
function buildAprUtil()
{
    mkdir -p $APR_UTIL_INSTALL
    tar -xvf ./apr-util-1.6.1.tar.gz 
    cd apr-util-1.6.1

    ./configure  --prefix=$APR_UTIL_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}
```

## 2.5编译APR-iconv 

使用 [Apache Portable Runtime](http://apr.apache.org/) 界面给出的版本：APR-iconv 1.2.2

下载地址：

```
https://apr.apache.org/download.cgi
```

编译：

```shell
function buildAprIconv()
{
    mkdir -p $APR_ICONV_INSTALL
    tar -xvf ./apr-iconv-1.2.2.tar.gz 
    cd apr-iconv-1.2.2

    ./configure  --prefix=$APR_ICONV_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}
```

## 2.6编译openssl

使用openssl官网发布的 openssl-1.1.1n.tar.gz

下载地址：

```
https://www.openssl.org/source/
```

编译：

```shell
function buildOpenssl
{
    mkdir -p $OPENSSL_INSTALL
    tar -xvf ./openssl-1.1.1n.tar.gz 
    cd ./openssl-1.1.1n/ 

    ./config --prefix=$OPENSSL_INSTALL 
    make 
    make install 
    cd ..
}
```

## 2.7编译ActiveMQ-Cpp

使用当前最新版本：activemq-cpp-library-3.9.5-src.tar.gz

下载地址：

```shell
https://activemq.apache.org/components/cms/download/
```

编译：

```shell
function buildActiveMqCpp
{
    mkdir -p $ACTIVEMQ_CPP_INSTALL
    tar -xvf ./activemq-cpp-library-3.9.5-src.tar.bz2 
    cd ./activemq-cpp-library-3.9.5/ 

    ./configure --prefix=$ACTIVEMQ_CPP_INSTALL --with-apr=$APR_INSTALL \
    --with-openssl=$OPENSSL_INSTALL --with-cppunit=$CPPUNIT_INSTALL
    make 
    make check
    make install 
    cd ..
}
```

# 3.编译脚本

完整编译脚本如下，除了上面的函数，还编写了顺序编译所有库和清理等函数：

```shell
#!/bin/bash 

CUR_PATH=`pwd`
INSTALL_PATH=$CUR_PATH/build/ #安装目录

LIBUUID_INSTALL=$INSTALL_PATH
CPPUNIT_INSTALL=$INSTALL_PATH
APR_INSTALL=$INSTALL_PATH
APR_UTIL_INSTALL=$APR_INSTALL
APR_ICONV_INSTALL=$APR_INSTALL
OPENSSL_INSTALL=$INSTALL_PATH
ACTIVEMQ_CPP_INSTALL=$INSTALL_PATH

#############################
#func  :libuuid 没有指定安装路径，将使用默认安装路径安装到系统中
#############################
function buildLibUUID()
{
    mkdir -p $LIBUUID_INSTALL #未使用
    tar -xvf  libuuid-1.0.3.tar.gz
    cd libuuid-1.0.3
    ./configure  #--prefix=$LIBUUID_INSTALL
    make -j
    make install
    cd ..
}

function buildCppUnit()
{
    mkdir -p $CPPUNIT_INSTALL
    tar -xvf  cppunit-1.12.1.tar.gz
    cd cppunit-1.12.1 

    ./configure  --prefix=$CPPUNIT_INSTALL
    make -j
    make install
    cd ..
}

function buildApr()
{
    mkdir -p $APR_INSTALL
    tar -xvf ./apr-1.7.0.tar.gz 
    cd apr-1.7.0

    ./configure  --prefix=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildAprUtil()
{
    mkdir -p $APR_UTIL_INSTALL
    tar -xvf ./apr-util-1.6.1.tar.gz 
    cd apr-util-1.6.1

    ./configure  --prefix=$APR_UTIL_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildAprIconv()
{
    mkdir -p $APR_ICONV_INSTALL
    tar -xvf ./apr-iconv-1.2.2.tar.gz 
    cd apr-iconv-1.2.2

    ./configure  --prefix=$APR_ICONV_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildOpenssl
{
    mkdir -p $OPENSSL_INSTALL
    tar -xvf ./openssl-1.1.1n.tar.gz 
    cd ./openssl-1.1.1n/ 

    ./config --prefix=$OPENSSL_INSTALL 
    make 
    make install 
    cd ..
}

function buildActiveMqCpp
{
    mkdir -p $ACTIVEMQ_CPP_INSTALL
    tar -xvf ./activemq-cpp-library-3.9.5-src.tar.bz2 
    cd ./activemq-cpp-library-3.9.5/ 

    ./configure --prefix=$ACTIVEMQ_CPP_INSTALL --with-apr=$APR_INSTALL \
    --with-openssl=$OPENSSL_INSTALL --with-cppunit=$CPPUNIT_INSTALL
    make 
    make check
    make install 
    cd ..
}

#############################
#func  :清除编译
#############################
function clean()
{
    rm -rf build
    rm -rf libuuid-1.0.3
    rm -rf cppunit-1.12.1 
    rm -rf apr-1.7.0
    rm -rf apr-util-1.6.1
    rm -rf apr-iconv-1.2.2
    rm -rf openssl-1.1.1n/ 
    rm -rf activemq-cpp-library-3.9.5/ 
}

function buildAll()
{
#   buildLibUUID
    buildCppUnit
    buildApr
    buildAprUtil
    buildAprIconv
    buildOpenssl
    buildActiveMqCpp
}

#########################
#func: 菜单
#########################
function menu()
{

    if [ $# -ne 1 ]
    then
        echo "usage : $0 build/clean"
        exit
    fi

    if   [ $1 = "build" ]
    then
        buildAll
    elif [ $1 = "clean" ]
    then
        clean
    else
        echo "usage : $0 build/clean"
    fi
}

function main()
{
    menu $*
}
main $*
```

执行` ./build.sh build`完成编译任务，并将成果物安装到当前目录下的build目录下

# 4.编译验证

将activemq-cpp-library-3.9.5/src/examples目录下的consumer目录拷贝到安装目录下，并进入拷贝后的目录：

```shell
cp activemq-cpp-library-3.9.5/src/examples/consumers/ ./build/ -raf
cd ./build/consumers/
```

编写makefile并编译

```makefile
TARGET=test
$(TARGET): *.cpp
	g++ $^ -o $@ \
        -pthread \
        -I../include \
        -I../include/activemq-cpp-3.9.5\
        -I../include/apr-1\
        -I../include/openssl\
        -L../lib \
        ../lib/libactivemq-cpp.a \
        -luuid \
        -I/usr/include/apr-1.0/ \
        -lssl \
        -lcrypto \
        -lapr-1 \
        -Wl,-rpath='$$ORIGIN:$$ORIGIN/../lib'

.PHONY:clean
clean:
	rm -rf $(TARGET)
```

编译通过，使用ldd 命令查看是否缺少依赖库:

```shell
$ ldd -r test

linux-vdso.so.1 (0x00007ffcab1c5000)
libssl.so.1.1 => /home/ActiveMQ-CppBuild/build/consumers/./../lib/libssl.so.1.1 (0x00007f8dc7906000)
libcrypto.so.1.1 => /home/ActiveMQ-CppBuild/build/consumers/./../lib/libcrypto.so.1.1 (0x00007f8dc7413000)
libapr-1.so.0 => /home/ActiveMQ-CppBuild/build/consumers/./../lib/libapr-1.so.0 (0x00007f8dc71d8000)
libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f8dc6e4f000)
libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f8dc6ab1000)
libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f8dc6899000)
libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f8dc667a000)
libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f8dc6289000)
libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f8dc6085000)
libuuid.so.1 => /lib/x86_64-linux-gnu/libuuid.so.1 (0x00007f8dc5e7e000)
/lib64/ld-linux-x86-64.so.2 (0x00007f8dc86e9000)
```

依赖库均已经找到，编译成功，可以运行程序了。