#!/bin/bash 

CUR_PATH=`pwd`
INSTALL_PATH=$CUR_PATH/build/ #安装目录

LIBUUID_INSTALL=$INSTALL_PATH
CPPUNIT_INSTALL=$INSTALL_PATH
APR_INSTALL=$INSTALL_PATH
APR_UTIL_INSTALL=$APR_INSTALL
APR_ICONV_INSTALL=$APR_INSTALL
OPENSSL_INSTALL=$INSTALL_PATH
ACTIVEMQ_CPP_INSTALL=$INSTALL_PATH

#############################
#func  :libuuid 没有指定安装路径，将使用默认安装路径安装到系统中
#############################
function buildLibUUID()
{
    mkdir -p $LIBUUID_INSTALL #未使用
    tar -xvf  libuuid-1.0.3.tar.gz
    cd libuuid-1.0.3
    ./configure  #--prefix=$LIBUUID_INSTALL
    make -j
    make install
    cd ..
}

function buildCppUnit()
{
    mkdir -p $CPPUNIT_INSTALL
    tar -xvf  cppunit-1.12.1.tar.gz
    cd cppunit-1.12.1 

    ./configure  --prefix=$CPPUNIT_INSTALL
    make -j
    make install
    cd ..
}

function buildApr()
{
    mkdir -p $APR_INSTALL
    tar -xvf ./apr-1.7.0.tar.gz 
    cd apr-1.7.0

    ./configure  --prefix=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildAprUtil()
{
    mkdir -p $APR_UTIL_INSTALL
    tar -xvf ./apr-util-1.6.1.tar.gz 
    cd apr-util-1.6.1

    ./configure  --prefix=$APR_UTIL_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildAprIconv()
{
    mkdir -p $APR_ICONV_INSTALL
    tar -xvf ./apr-iconv-1.2.2.tar.gz 
    cd apr-iconv-1.2.2

    ./configure  --prefix=$APR_ICONV_INSTALL  --with-apr=$APR_INSTALL
    make -j
    make install
    cd ..
}

function buildOpenssl
{
    mkdir -p $OPENSSL_INSTALL
    tar -xvf ./openssl-1.1.1n.tar.gz 
    cd ./openssl-1.1.1n/ 

    ./config --prefix=$OPENSSL_INSTALL 
    make 
    make install 
    cd ..
}

function buildActiveMqCpp
{
    mkdir -p $ACTIVEMQ_CPP_INSTALL
    tar -xvf ./activemq-cpp-library-3.9.5-src.tar.bz2 
    cd ./activemq-cpp-library-3.9.5/ 

    ./configure --prefix=$ACTIVEMQ_CPP_INSTALL --with-apr=$APR_INSTALL \
    --with-openssl=$OPENSSL_INSTALL --with-cppunit=$CPPUNIT_INSTALL
    make 
    make check
    make install 
    cd ..
}

#############################
#func  :清除编译
#############################
function clean()
{
    rm -rf build
    rm -rf libuuid-1.0.3
    rm -rf cppunit-1.12.1 
    rm -rf apr-1.7.0
    rm -rf apr-util-1.6.1
    rm -rf apr-iconv-1.2.2
    rm -rf openssl-1.1.1n/ 
    rm -rf activemq-cpp-library-3.9.5/ 
}

function buildAll()
{
#   buildLibUUID
    buildCppUnit
    buildApr
    buildAprUtil
    buildAprIconv
    buildOpenssl
    buildActiveMqCpp
}

#########################
#func: 菜单
#########################
function menu()
{

    if [ $# -ne 1 ]
    then
        echo "usage : $0 build/clean"
        exit
    fi

    if   [ $1 = "build" ]
    then
        buildAll
    elif [ $1 = "clean" ]
    then
        clean
    else
        echo "usage : $0 build/clean"
    fi
}

function main()
{
    menu $*
}
main $*
