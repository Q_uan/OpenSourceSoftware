# 1.页面介绍

## 1.1 主界面

在主界面可以看到broker的上电运行时间（Uptime）

![home](./pic/home.png)

## 1.2 queue消息队列页面 (点对点模式)

- Name : 消息队列名称
- Number Of Pending Messages: 未被消费的消息，**对于持久化的未被消费的消息，broker重启之后，未被消费的数目将显示在这里**
- Number Of Consumers:消费者数量
- Messages Enqueued :进队列的消息数。进入队列的消息总消息数目,包括已被消费和未被消费的消息数目.这个 数目只增不减
- Messages Dequeued :出队列的消息数。可以理解为是被消费的消息数量,在queues里和它进入队列的总数量相等(因为一个消息之后被成功消费一次.)如果暂时不等,是因为消费者还没来得及消费

![queue](./pic/queue.png)

## 1.3 topic主题页面(发布订阅模式)

Name : 主题名称
Number Of Consumers:消费者数量
Messages Enqueued : 入队列的消息数。进入队列的消息总数目，包括已被消费和未被消费的,这个数目只增不减
Messages Dequeued :出队列的消息数。可以理解为是被消费的掉的消息数量，在topics里,因为多消费者从而导致数量会比入队数目要高

![topic](./pic/topic.png)

## 1.4 topic subscriber

这个界面可以查看和创建topic订阅者

- Active Durable Topic Subscribers ：激活状态的持久化topic订阅者
- Offline Durable Topic Subscribers：不在线的持久化topic订阅者
- Active Non-Durable Topic Subscribers：激活状态的非持久化topic订阅者

![topicSubscribers](./pic/topicSubscribers.png)

## 1.5 发送界面

- Destination：消息Name
- Queue or Topic：消息类型选择
- Persistent Delivery：持久化投递选项
- Message body：消息内容
- send ：发送按钮

![send](./pic/send.png)

# 2.操作实践

这一节在不编写代码的情况下，通过ActiveMq界面操作，进一步加深对ActiveMq的了解。

## 2.1 发送非持久化topic消息并观察

### 2.1.1创建topic ：

![createTopic](./pic/createTopic.png)

### 2.1.2发送消息：

![sendATopic](./pic/sendATopic.png)



### 2.1.3观察入队出队数：

发送了一条消息，Messages Enqueued 为1，由于没有消费者，Number of Consumers为0，出队数也为0。

![topicEnqueued](./pic/topicEnqueued.png)

## 2.2 发送持久化和非持久化queue消息并观察

### 2.2.1创建queue ：

![createQueue](./pic/createQueue.png)



### 2.2.2发送非持久化queue消息：

![image-20220409101825613](./pic/sendNoPersistentQueenMsg.png)



发送完成后，在Queues界面可以看到Number Of Pending Messages为1，入队数为1，消费者数为0，出队数为0.

### 2.2.3重启broker

通过命令行重启activemq服务，然后重新进入web界面。

### 2.2.4观察：

重启之后，在Queues界面可以看到所有数据均为清0，说明对于非持久化的queue消息，重启broker之后，没有消费的消息并没有被恢复。

![queuesWebAfterReboot](./pic/queuesWebAfterReboot.png)

### 2.2.5发送持久化消息

![sendPersistentQueenMsg](./pic/sendPersistentQueenMsg.png)

### 2.2.6重启broker：

通过命令行重启activemq服务，然后重新进入web界面。

### 2.2.7观察：

重启之后，在Queues界面可以看到出入队数被清0，但是Number Of Pending Messages为1，说明对于持久化的消息，重启broker之后会恢复消息。

![queuesWebAfterReboot2](./pic/queuesWebAfterReboot2.png)

## 2.3 创建持久的topic订阅者

在topic subscriber界面创建持久的topic订阅者
