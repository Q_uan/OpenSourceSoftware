#ifndef _CURL_HTTP_CLIENT_H_
#define _CURL_HTTP_CLIENT_H_

#include <string>
#include <list>
#include "curl/curl.h"      /* 引入http  服务接口 */

typedef size_t (*WriteDataCBFunc)(void* buffer, size_t size, size_t nmemb, void* lpVoid);

class CurlHttpClient    // http client 基类
{
public:
    struct HttpKeyVal
    {
        std::string key;
        std::string value;
    };
public:
    CurlHttpClient(int format = 0);
    CurlHttpClient(std::string username, std::string password);
    virtual ~CurlHttpClient(void);

    /** 
    * @brief HTTP POST请求
    * @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
    * @param strPost 输入参数,使用如下格式para1=val1?2=val2&…
    * @param strResponse 输出参数,返回的内容
    * @return 返回是否Post成功
    */
    virtual int Post(const std::string & strUrl, long & httpCode, const std::string & strPost, std::string & strResponse);

    /** 
    * @brief HTTP GET请求
    * @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
    * @param strResponse 输出参数,返回的内容
    * @return 返回是否Post成功
    */
    virtual int Get(const std::string & strUrl, long & httpCode, const std::string & strGet, std::string & strResponse);
    virtual int Get(const std::string & strUrl, long & httpCode, const std::string & strGet, std::string & strResponse, std::list<HttpKeyVal> & resHeaders);
    /** 
    * @brief HTTP Put请求
    * @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
    * @param strPut 输入参数,使用如下格式para1=val1?2=val2&…
    * @param strResponse 输出参数,返回的内容
    * @return 返回是否Put成功
    */
    virtual int Put(const std::string & strUrl, long & httpCode, const std::string & strPut, std::string & strResponse);
    
    /** 
     * @brief HTTP Get Download 数据
     * @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
     * @param writeDataFunc 数据回调函数
     * @return 返回是否Get成功
     */
    virtual int Download(const std::string & strUrl, long & httpCode, const std::string & strGet, WriteDataCBFunc cbWriteDataFunc, void *lpVoid);

    /** 
    * @brief HTTPS POST请求,无证书版本
    * @param strUrl 输入参数,请求的Url地址,如:https://www.alipay.com
    * @param strPost 输入参数,使用如下格式para1=val1?2=val2&…
    * @param strResponse 输出参数,返回的内容
    * @param pCaPath 输入参数,为CA证书的路径.如果输入为NULL,则不验证服务器端证书的有效性.
    * @return 返回是否Post成功
    */
    virtual int Posts(const std::string & strUrl, long & httpCode, const std::string & strPost, std::string & strResponse, const char * pCaPath = NULL);

    /** 
    * @brief HTTPS GET请求,无证书版本
    * @param strUrl 输入参数,请求的Url地址,如:https://www.alipay.com
    * @param strResponse 输出参数,返回的内容
    * @param pCaPath 输入参数,为CA证书的路径.如果输入为NULL,则不验证服务器端证书的有效性.
    * @return 返回是否Post成功
    */
    virtual int Gets(const std::string & strUrl, long & httpCode, std::string & strResponse, const char * pCaPath = NULL);

    /** 
    * @brief HTTPS DELETE请求,无证书版本
    * @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
    * @param strPut 输入参数,使用如下格式para1=val1?2=val2&…
    * @param strResponse 输出参数,返回的内容
    * @return 返回是否Delete成功
    */
    virtual int Delete(const std::string & strUrl, long & httpCode, const std::string & strDelete, std::string & strResponse);

    void SetDebug(bool bDebug);
    void SetTimeout(int timeout);
    void SetVerbose(int verbose){m_verbose = verbose;};
    void AddHeader(std::string & headText);
    void ClearHeaders();
    void setAuthMode(unsigned long authMap);
    int checkAuthMode(unsigned long authMap);

private:
    bool m_bDebug;

    unsigned long m_httpAuthMod;      // 要使用的http鉴权方式 为curl库提供
    std::string m_username;
    std::string m_password;

    int m_timeout;
    int m_verbose;
private:
    struct curl_slist* m_headers;
private:
    static volatile bool m_globalInitFlag;/* 初始化libcurl运行环境标志位 */
private:
    static int globalInit();
};



#endif
