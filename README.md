这个目录中存放了开源软件的使用demo

有些目录只有代码没有markdown文件

这里列出了目录中的markdown文件:

-   [1.ActiveMQ在linux下的安装.md](./ActiveMQ/1.ActiveMQ在linux下的安装.md)

-   [1.Doxygen在linux下的安装与测试.md](./DoxygenDemo/1_Doxygen在linux下的安装与测试/1.Doxygen在linux下的安装与测试.md)

-   [1.glog在linux下的安装与测试.md](./glogDemo/1_glog的编译安装及测试/1.glog在linux下的安装与测试.md)

-   [linux下源码安装googletest.md](./googletestDir/1_linux下源码安装googletest/linux下源码安装googletest.md)

-   [CC++实现的ini解析库-inih.md](./inihDemo/1_INIReaderTest/CC++实现的ini解析库-inih.md)

-   [1.Linux下安装jdk1.8.md](./jdk/1.Linux下安装jdk1.8.md)

-   [linux下源码编译jsoncpp.md](./jsoncpp/1_linux下源码编译jsoncpp/linux下源码编译jsoncpp.md)

-   [1.libevent在linux的编译安装.md](./libeventDir/1_libevent在linux的编译安装/1.libevent在linux的编译安装.md)

-   [2.基于libevent的httpServer_C++版本.md](./libeventDir/2_基于libevent的httpServer_C++版本/2.基于libevent的httpServer_C++版本.md)

-   [1.lrzsz介绍及安装.md](./lrzsz/1.lrzsz介绍及安装.md)

-   [README.md](./mongooseDir/httpServer/README.md)

-   [1.OpenCV3.4.6命令行编译.md](./OpenCV/1.OpenCV3.4.6命令行编译.md)

-   [1.protobuf编译和测试.md](./protobufDir/1_protobuf编译和测试/1.protobuf编译和测试.md)

-   [README.md](./README.md)

-   [1.XML介绍及tinyxml2的使用.md](./tinyxml2/1.XML介绍及tinyxml2的使用.md)

