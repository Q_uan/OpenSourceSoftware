#ifndef _BLOCKING_QUEUE_H_
#define _BLOCKING_QUEUE_H_

#include <deque>
#include <mutex>
#include <condition_variable>

template<typename T>
class BlockingQueue
{
  std::deque<T> content;
  size_t capacity;

  std::mutex mutex;
  std::condition_variable not_empty;
  std::condition_variable not_full;

  BlockingQueue(const BlockingQueue &) = delete;
  BlockingQueue(BlockingQueue &&) = delete;
  BlockingQueue &operator = (const BlockingQueue &) = delete;
  BlockingQueue &operator = (BlockingQueue &&) = delete;

 public:
  BlockingQueue(size_t capacity): capacity(capacity) {}

  void push(T &&item) {
    {
      std::unique_lock<std::mutex> lk(mutex);
      not_full.wait(lk, [this]() { return content.size() < capacity; });
      content.push_back(std::move(item));
    }
    not_empty.notify_one();
  }

  bool try_push(T &&item) {
    {
      std::unique_lock<std::mutex> lk(mutex);
      if (content.size() == capacity)
        return false;
      content.push_back(std::move(item));
    }
    not_empty.notify_one();
    return true;
  }

    bool try_push(T &&item, unsigned int millisecondsNum) 
    {
        {
            std::unique_lock<std::mutex> lk(mutex);
            bool ret = not_full.wait_for(lk, std::chrono::milliseconds(millisecondsNum), [this]() { return content.size() < capacity; });
            if (false == ret)
            {
                return false;
            }
            content.push_back(std::move(item));
        }
        not_empty.notify_one();
        return true;
    }
  void pop(T &item) {
    {
      std::unique_lock<std::mutex> lk(mutex);
      not_empty.wait(lk, [this]() { return !content.empty(); });
      item = std::move(content.front());
      content.pop_front();
    }
    not_full.notify_one();
  }

  bool try_pop(T &item) {
    {
      std::unique_lock<std::mutex> lk(mutex);
      if (content.empty())
        return false;
      item = std::move(content.front());
      content.pop_front();
    }
    not_full.notify_one();
    return true;
  }
};

#endif
