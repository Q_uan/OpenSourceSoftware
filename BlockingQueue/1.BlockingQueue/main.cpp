#include "BlockingQueue.hpp"
#include <iostream>
#include <memory>
#include <unistd.h>
#include <thread>

class Test
{
public:
    Test()
    {
        std::cout<<"Test()"<<std::endl;
    }
    ~Test()
    {
        std::cout<<"~Test()"<<std::endl;
    }
private:
    int m_val;
};

BlockingQueue<std::unique_ptr<Test> > queue(5);

int main(int argc, const char *argv[])
{
    std::thread th([&]()->void {
        while(true)
        {
            {
                std::unique_ptr<Test> ptr;
                queue.pop(ptr);
            }
        }

    });

    for(int i = 0; i < 5; i++)
    {
        std::unique_ptr<Test> ptr(new Test());
        queue.try_push(std::move(ptr));
        sleep(1);
    }

    std::cout<<"exit"<<std::endl;
    sleep(1);

    return 0;
}
